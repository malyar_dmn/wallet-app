import { Navigate, Route, Routes } from "react-router-dom";
import { AddCardPage } from "../pages/AddCardPage";
import { WalletsPage } from "../pages/WalletPage";

export const AppRouter = () => {
  return (
    <Routes>
      <Route path="/" element={<WalletsPage />} />
      <Route path="/add-card" element={<AddCardPage />} />
      <Route path="/edit-card/:id" element={<AddCardPage />} />
      <Route path="*" element={<Navigate to={"/"} />} />
    </Routes>
  );
};
