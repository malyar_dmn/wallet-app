import axios from "axios";

export const binlookup = (cardNumber: string) => {
  const cardNumberEndpoint = cardNumber.slice(0, 8);

  return axios
    .get(`https://lookup.binlist.net/${cardNumberEndpoint}`)
    .then((res) => {
      return {
        bankName: res.data.bank?.name,
        schema: res.data.scheme,
        type: res.data.type,
      };
    });
};
