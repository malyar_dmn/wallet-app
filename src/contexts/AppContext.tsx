import { createContext, FC, ReactNode, useContext, useState } from "react";
import { IBankCard } from "../types/IBankCard";
import { enumToArrayValues } from "../common/helpers";
import { CurrencyNames } from "../types/Constants";
import { CashObjectType } from "../types/types";

export type ICashState = Record<string, number>;

export interface GlobalState {
  bankCards: IBankCard[];
  cash: ICashState;
  activeCard: string;
}

export interface GlobalController {
  state: GlobalState;
  addCash(cashObj: CashObjectType): void;
  editCash(cashObj: CashObjectType): void;
  addCard(cardData: IBankCard): void;
  removeCard(cardNumber: string): void;
  getTotalBalance(): CashObjectType[];
  selectCard(cardNumber: string): void;
  editCard(cardData: IBankCard): void;
  getCardByNumber(cardNumber: string): IBankCard | undefined;
}

const AppContext = createContext<GlobalController>({} as GlobalController);

interface Props {
  children: ReactNode;
}

export const useDataContext = () => useContext<GlobalController>(AppContext);

export const AppContextProvider: FC<Props> = ({ children }) => {
  const [data, setData] = useState<GlobalState>({
    bankCards: [],
    cash: {},
    activeCard: "",
  });

  const getCardsTotals = () => {
    return data.bankCards.reduce(
      (acc: Record<string, number>, card: IBankCard) => {
        const { currency, cardBalance } = card;
        if (acc[currency]) {
          acc[currency] += +cardBalance;
        } else {
          acc[currency] = +cardBalance;
        }
        return acc;
      },
      {}
    );
  };

  const getTotalBalance = () => {
    const cardTotal = getCardsTotals();

    return enumToArrayValues(CurrencyNames).map((currency: string) => {
      const cardSum = cardTotal[currency] || 0;
      const cashSum = data.cash[currency] || 0;
      return {
        // [currency]: cardSum + cashSum,
        currency,
        amount: cardSum + cashSum,
      };
    });
  };

  const addCash = (cashObj: CashObjectType) => {
    setData((prev) => {
      return {
        ...prev,
        cash: {
          ...prev.cash,
          [cashObj.currency]: prev.cash[cashObj.currency]
            ? cashObj.amount + prev.cash[cashObj.currency]
            : cashObj.amount,
        },
      };
    });
  };

  const editCash = (cashObj: CashObjectType) => {
    setData((prev) => {
      return {
        ...prev,
        cash: {
          ...prev.cash,
          [cashObj.currency]: cashObj.amount,
        },
      };
    });
  };

  const addCard = (cardData: IBankCard) => {
    setData((prev) => {
      return {
        ...prev,
        bankCards: [...prev.bankCards, cardData],
      };
    });
  };

  const editCard = (cardData: IBankCard) => {
    setData((prev) => {
      return {
        ...prev,
        bankCards: [
          ...prev.bankCards.filter(
            (card) => card.cardNumber !== cardData.cardNumber
          ),
          cardData,
        ],
      };
    });
  };

  const removeCard = (cardNumber: string) => {
    setData((prev) => {
      return {
        ...prev,
        bankCards: [
          ...prev.bankCards.filter((card) => card.cardNumber !== cardNumber),
        ],
      };
    });
  };

  const selectCard = (cardNumber: string) => {
    setData((prev) => {
      return {
        ...prev,
        activeCard: cardNumber,
      };
    });
  };

  const getCardByNumber = (cardNumber: string): IBankCard | undefined => {
    const card = data.bankCards.find((card) => card.cardNumber === cardNumber);
    if (card) {
      return card;
    }
  };

  const globalState: GlobalController = {
    state: data,
    addCash,
    editCash,
    addCard,
    removeCard,
    getTotalBalance,
    selectCard,
    editCard,
    getCardByNumber,
  };

  return (
    <AppContext.Provider value={globalState}>{children}</AppContext.Provider>
  );
};
