export const enumToArrayValues = <T>(e: { [s: string]: T }) => {
  return Object.entries(e).map(([key, value]) => value);
};

export const luhnValidation = (card: string | undefined): boolean => {
  if (!card) return false;
  let checksum = 0;
  const cardnumbers = card.split("").map((el) => Number(el));

  for (const [index, num] of Array.from(cardnumbers.entries())) {
    if (index % 2 === 0) {
      let buffer = num * 2;
      buffer > 9 ? (checksum += buffer - 9) : (checksum += buffer);
    } else {
      checksum += num;
    }
  }
  return checksum % 10 === 0 ? true : false;
};

function clearNumber(value = "") {
  return value.replace(/\D+/g, "");
}

export function formatExpirationDate(value: string) {
  const clearValue = clearNumber(value);

  if (clearValue.length >= 3) {
    let month = clearValue.slice(0, 2);
    let year = clearValue.slice(2, 4);

    if (+month < 0) {
      month = "0";
    } else if (+month > 12) {
      month = "12";
    }

    return `${month}/${year}`;
  }

  return clearValue;
}

export const separateMask = (value: string) => {
  return value.split("").reduce((acc, el, index) => {
    if (index !== 0 && index % 4 === 0) {
      acc += " ";
    }
    acc += el;
    return acc;
  });
};

export const hideMask = (value: string) => {
  return value.split("").reduce((acc, el, index) => {
    if (index !== 0 && index % 4 === 0) {
      acc += " ";
    }

    if (index > 3 && index < 12) {
      acc += "*";
    } else {
      acc += el;
    }

    return acc;
  });
};

export const clearSpaces = (str: string) => {
  return str.replace(/\s+/g, "");
};
