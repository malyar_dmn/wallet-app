import { Container } from "@mui/material";
import axios from "axios";
import React, { useEffect } from "react";
import "./App.css";
import { AppRouter } from "./router/AppRouter";

function App() {
  return (
    <div className="App">
      <Container
        maxWidth="md"
        sx={{ border: "1px solid #ccc", borderRadius: "5px" }}
      >
        <AppRouter />
      </Container>
    </div>
  );
}

export default App;
