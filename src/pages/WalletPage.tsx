import { Button, Card, Grid, Typography } from "@mui/material";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { CashModal } from "../components/CashModal";
import { BankCard } from "../components/BankCard";
import { CashDashboard } from "../components/CashDashboard";
import { useDataContext } from "../contexts/AppContext";

export const WalletsPage = () => {
  const { state } = useDataContext();
  const navigate = useNavigate();
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

  const handleOpenModal = () => {
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  return (
    <Grid
      container
      style={{ padding: "15px" }}
      sx={{
        flexDirection: { xs: "column", md: "row" },
        alignItems: { xs: "center", md: "stretch" },
      }}
    >
      <Grid
        item
        container
        xs={6}
        justifyContent="space-between"
        alignItems="stretch"
        style={{ width: "100%" }}
      >
        <CashDashboard />
      </Grid>
      <Grid
        item
        container
        xs={6}
        justifyContent="center"
        sx={{
          marginTop: { xs: "20px", md: "0px" },
        }}
      >
        <Grid
          item
          container
          xs={12}
          sx={{ justifyContent: { md: "flex-start", xs: "center" } }}
        >
          <Grid item>
            <Button
              variant="outlined"
              color="success"
              onClick={() => navigate("/add-card")}
            >
              Додати картку
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="outlined"
              color="success"
              onClick={handleOpenModal}
            >
              Додати готівку
            </Button>
          </Grid>
        </Grid>
        <Grid
          item
          container
          marginTop={2}
          gap={2}
          sx={{ justifyContent: { md: "flex-start", xs: "center" } }}
        >
          {state.bankCards.length <= 0 && (
            <Typography>У вас немає карт!</Typography>
          )}
          {state.bankCards.map((card) => (
            <BankCard cardData={card} key={card.cardNumber} />
          ))}
        </Grid>
        <CashModal isOpen={isModalOpen} handleClose={handleCloseModal} />
      </Grid>
    </Grid>
  );
};
