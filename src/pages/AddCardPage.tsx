import { Grid } from "@mui/material";
import { useParams } from "react-router-dom";
import * as yup from "yup";
import { CardForm } from "../components/CardForm";
import { CashDashboard } from "../components/CashDashboard";

type Params = {
  id: string | undefined;
};

export const AddCardPage = () => {
  const { id } = useParams<Params>();
  return (
    <Grid
      container
      style={{ padding: "15px" }}
      sx={{
        flexDirection: { md: "row", xs: "column" },
        alignItems: { md: "stretch", xs: "center" },
      }}
    >
      <Grid item container xs={6} justifyContent="space-between">
        <CashDashboard />
      </Grid>
      <Grid item xs={6} justifyContent="center">
        <CardForm id={id} />
      </Grid>
    </Grid>
  );
};
