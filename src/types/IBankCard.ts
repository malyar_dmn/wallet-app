export interface IBankCard {
  bankName: string;
  cardNumber: string;
  type: string;
  schema: string;
  expireDate: string;
  ccvCode: string;
  cardBalance: string;
  currency: string;
}
