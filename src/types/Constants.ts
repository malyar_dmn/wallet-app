export enum PaymentSystemTypes {
  VISA = "visa",
  MASTER_CARD = "master card",
}

export enum CurrencyNames {
  UAH = "UAH",
  USD = "USD",
  EUR = "EUR",
}
