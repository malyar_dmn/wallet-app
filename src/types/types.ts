export type CashObjectType = {
  currency: string;
  amount: number;
};
