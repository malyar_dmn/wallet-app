import { Button, Grid, MenuItem, TextField } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useFormik } from "formik";
import { ChangeEvent, FC } from "react";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import { binlookup } from "../api/binlookup";
import {
  clearSpaces,
  enumToArrayValues,
  formatExpirationDate,
  luhnValidation,
} from "../common/helpers";
import { useDataContext } from "../contexts/AppContext";
import { CurrencyNames } from "../types/Constants";
import { IBankCard } from "../types/IBankCard";

const useStyles = makeStyles({
  formInput: {
    width: "100%",
  },
  formSubmitButtons: {
    width: "100%",
  },
});

const validationSchema = yup.object({
  cardNumber: yup
    .string()
    .length(16, "Значення повинно містити 16 символів")
    .typeError("Номер карти повинен складатися з 16 цифр")
    .test("luhn-validation", "Значення карти не валідне", (cardNumber) =>
      luhnValidation(cardNumber)
    )
    .required(),
  expirationDate: yup.string().required("Значення має бути заповненим"),
  cvv: yup
    .string()
    .test("length", "Значення має бути 3 - значним числом", (value) =>
      value ? value.length === 3 : false
    )
    .required("Значення має бути заповненим"),
  cardHolder: yup.string().notRequired(),
  amount: yup
    .number()
    .required("Значення має бути заповнен")
    .typeError("Значення поля має бути числовим"),
  currency: yup.string().required("Значення має бути обрано"),
});

const currencies = enumToArrayValues(CurrencyNames);

interface Props {
  cardFormData?: IBankCard;
  id?: string | undefined;
}

type CardFormInitValueType = {
  cardNumber: string;
  expirationDate: string;
  cvv: string;
  cardHolder: string;
  amount: string;
  currency: string;
};

export const CardForm: FC<Props> = (props) => {
  const navigate = useNavigate();
  const { addCard, editCard, getCardByNumber } = useDataContext();
  const existingCard = getCardByNumber(props.id!);

  const classes = useStyles();

  const formik = useFormik<CardFormInitValueType>({
    initialValues: {
      cardNumber: existingCard?.cardNumber || "",
      expirationDate: existingCard?.expireDate || "",
      cvv: existingCard?.ccvCode || "",
      cardHolder: "",
      amount: existingCard?.cardBalance || "",
      currency: existingCard?.currency || "",
    },
    validationSchema: validationSchema,
    onSubmit: async (values) => {
      const cardDataObject = await createCardObject(values);
      if (props.id) {
        editCard(cardDataObject);
      } else {
        addCard(cardDataObject);
      }

      navigate("/");
    },
  });

  const createCardObject = async (formValues: CardFormInitValueType) => {
    const binResult = await binlookup(formValues.cardNumber);
    const cardDataObject: IBankCard = {
      bankName: binResult.bankName,
      cardBalance: formValues.amount,
      cardNumber: formValues.cardNumber,
      ccvCode: formValues.cvv,
      currency: formValues.currency,
      expireDate: formValues.expirationDate,
      schema: binResult.schema,
      type: binResult.type,
    };
    return cardDataObject;
  };

  const handleChangeCardNumber = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const newValue = clearSpaces(value);
    formik.setFieldValue("cardNumber", newValue);
  };

  const handleChangeExpirationDate = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const newValue = formatExpirationDate(value);
    formik.setFieldValue("expirationDate", newValue);
  };

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container alignItems="center" rowSpacing={2}>
        <Grid item xs={12}>
          <TextField
            className={classes.formInput}
            placeholder="Card number"
            name="cardNumber"
            disabled={!!existingCard}
            helperText={formik.errors.cardNumber}
            error={!!formik.errors.cardNumber && formik.touched.cardNumber}
            value={formik.values.cardNumber}
            onChange={handleChangeCardNumber}
          />
        </Grid>
        <Grid item container columnSpacing={2}>
          <Grid item xs={6}>
            <TextField
              className={classes.formInput}
              placeholder="Expiration date"
              name="expirationDate"
              disabled={!!existingCard}
              helperText={formik.errors.expirationDate}
              error={
                !!formik.errors.expirationDate && formik.touched.expirationDate
              }
              value={formik.values.expirationDate}
              onChange={handleChangeExpirationDate}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              className={classes.formInput}
              placeholder="CVV"
              name="cvv"
              disabled={!!existingCard}
              value={formik.values.cvv}
              helperText={formik.errors.cvv}
              error={!!formik.errors.cvv && formik.touched.cvv}
              type="text"
              inputProps={{ minLength: 3, maxLength: 3 }}
              onChange={formik.handleChange}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <TextField
            className={classes.formInput}
            placeholder="Card holder"
            name="cardHolder"
            helperText={formik.errors.cardHolder}
            error={!!formik.errors.cardHolder && formik.touched.cardHolder}
            value={formik.values.cardHolder}
            onChange={formik.handleChange}
          />
        </Grid>
        <Grid item container columnSpacing={2}>
          <Grid item xs={6}>
            <TextField
              className={classes.formInput}
              placeholder="Amount"
              name="amount"
              helperText={formik.errors.amount}
              error={!!formik.errors.amount && formik.touched.amount}
              value={formik.values.amount}
              onChange={formik.handleChange}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              id="currency"
              name="currency"
              select
              label="Currency"
              onChange={formik.handleChange}
              helperText={formik.errors.currency}
              error={!!formik.errors.currency && formik.touched.currency}
              value={formik.values.currency}
              className={classes.formInput}
            >
              {currencies.map((option) => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
        </Grid>
        <Grid item container xs={12}>
          <Grid item xs={6}>
            <Button
              type="submit"
              className={classes.formSubmitButtons}
              disabled={!(formik.isValid && formik.dirty)}
            >
              {props.id ? "Редагувати" : "Додати"}
            </Button>
          </Grid>
          <Grid item xs={6}>
            <Button
              className={classes.formSubmitButtons}
              onClick={() => navigate("/")}
            >
              Скасувати
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
};
