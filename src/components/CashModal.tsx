import { Button, Grid, Input, MenuItem, Modal, TextField } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { FC } from "react";
import { enumToArrayValues } from "../common/helpers";
import * as yup from "yup";
import { useFormik } from "formik";
import { useDataContext } from "../contexts/AppContext";
import { CurrencyNames } from "../types/Constants";
import { CashObjectType } from "../types/types";

interface Props {
  isOpen: boolean;
  handleClose(): void;
  editCashData?: CashObjectType;
}

const useStyles = makeStyles({
  modal: {
    position: "absolute",
    left: "50%",
    top: "50%",
    maxWidth: "300px",
    transform: "translate(-50%, -50%)",
    padding: "15px",
    borderRadius: "5px",
    backgroundColor: "#fff",
  },
  modalInput: {
    width: "100%",
  },
});

const currencies = enumToArrayValues(CurrencyNames);

const validationSchema = yup.object({
  amount: yup.number().required(),
  currency: yup.string().required(),
});

export const CashModal: FC<Props> = (props) => {
  const { isOpen, handleClose, editCashData } = props;
  const { addCash, editCash } = useDataContext();
  const classes = useStyles();

  const formik = useFormik({
    initialValues: {
      amount: editCashData?.amount || 0,
      currency: editCashData?.currency || "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      const cashObject: CashObjectType = {
        amount: +values.amount,
        currency: values.currency,
      };
      if (editCashData) {
        editCash(cashObject);
      } else {
        addCash(cashObject);
      }
      formik.resetForm();
      handleClose();
    },
  });

  return (
    <Modal
      disableAutoFocus
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Grid container className={classes.modal} rowSpacing={2}>
        <form onSubmit={formik.handleSubmit}>
          <Grid item container direction="row" columnSpacing={2}>
            <Grid item container xs={6} justifyContent="center">
              <Input
                id="amount"
                name="amount"
                value={formik.values.amount}
                onChange={formik.handleChange}
              />
            </Grid>
            <Grid item container xs={6} justifyContent="center">
              <TextField
                id="currency"
                name="currency"
                select
                size="small"
                label="Currency"
                onChange={formik.handleChange}
                value={formik.values.currency}
                disabled={!!editCashData}
                className={classes.modalInput}
              >
                {currencies.map((option) => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
          </Grid>
          <Grid item container direction="row">
            <Grid item container xs={6} justifyContent="center">
              <Button type="submit">Зберегти</Button>
            </Grid>
            <Grid item container xs={6} justifyContent="center">
              <Button onClick={handleClose}>Скасувати</Button>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </Modal>
  );
};
