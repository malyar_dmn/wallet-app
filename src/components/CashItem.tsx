import { Button, Grid, Typography } from "@mui/material";
import { FC, useState } from "react";
import { CashObjectType } from "../types/types";
import { CashModal } from "./CashModal";

interface Props {
  cashObj: CashObjectType;
}

export const CashItem: FC<Props> = (props) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const { cashObj } = props;

  const handleCloseModal = () => setIsOpen(false);
  const handleOpenModal = () => setIsOpen(true);

  return (
    <Grid item container direction="row" alignItems="center">
      <Grid item xs={5}>
        <Typography align="left">
          - {cashObj.amount} {cashObj.currency}
        </Typography>
      </Grid>
      <Grid item xs={7}>
        <Button onClick={handleOpenModal}>Редагувати</Button>
      </Grid>
      <CashModal
        editCashData={cashObj}
        handleClose={handleCloseModal}
        isOpen={isOpen}
      />
    </Grid>
  );
};
