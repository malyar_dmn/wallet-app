import { Grid, Typography } from "@mui/material";
import { useDataContext } from "../contexts/AppContext";
import { CardDashboardItem } from "./CardDashboardItem";
import { CashItem } from "./CashItem";

export const CashDashboard = () => {
  const { state, getTotalBalance } = useDataContext();
  const cashObjArray = Object.keys(state.cash).map((key) => {
    return {
      currency: key,
      amount: state.cash[key],
    };
  });
  const balanceTotal = getTotalBalance();
  return (
    <Grid container direction="column" justifyContent="space-between">
      <Grid item container direction="column" spacing={1}>
        <Grid item>
          <Typography align="left">Баланс</Typography>
        </Grid>
        <Grid item>
          {balanceTotal.map((item) => (
            <Typography align="left" key={item.currency}>
              - {item.amount} {item.currency}
            </Typography>
          ))}
        </Grid>
      </Grid>
      <Grid
        item
        container
        spacing={1}
        direction="column"
        sx={{ margin: { md: "0", xs: "20px 0" } }}
      >
        <Grid item>
          <Typography align="left">Готівка</Typography>
        </Grid>
        <Grid item>
          {cashObjArray.length <= 0 && <Typography>- 0 UAH</Typography>}
          {cashObjArray.map((item) => (
            <CashItem cashObj={item} key={item.currency} />
          ))}
        </Grid>
      </Grid>
      <Grid item container spacing={1}>
        <Grid item>
          <Typography align="left">Мої картки</Typography>
        </Grid>
        <Grid item container>
          {state.bankCards.length <= 0 && <Typography>Карток немає</Typography>}
          {state.bankCards.map((card) => (
            <CardDashboardItem card={card} key={card.cardNumber} />
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};
