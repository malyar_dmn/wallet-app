import { FaCcMastercard, FaCcVisa } from "react-icons/fa";
import {
  Button,
  Card,
  CardActionArea,
  CardContent,
  Grid,
  Typography,
} from "@mui/material";
import { FC } from "react";
import { useDataContext } from "../contexts/AppContext";
import { IBankCard } from "../types/IBankCard";
import { PaymentSystemTypes } from "../types/Constants";
import { hideMask, separateMask } from "../common/helpers";

interface Props {
  cardData: IBankCard;
}

export const BankCard: FC<Props> = (props) => {
  const { cardData } = props;
  const { state, removeCard, selectCard } = useDataContext();
  const isActive = state.activeCard === cardData.cardNumber ? true : false;

  const renderIcon = (paymentSystemName: string | undefined) => {
    if (paymentSystemName === PaymentSystemTypes.MASTER_CARD) {
      return <FaCcMastercard size={25} />;
    } else if (paymentSystemName === PaymentSystemTypes.VISA) {
      return <FaCcVisa size={25} />;
    } else {
      return <Typography>{cardData.schema}</Typography>;
    }
  };

  const handleClick = () => {
    selectCard(cardData.cardNumber);
  };

  return (
    <Grid item container>
      <Grid item>
        <Card sx={{ minWidth: "250px" }} raised={isActive}>
          <CardActionArea onClick={handleClick}>
            <CardContent sx={{ textAlign: "left" }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <div style={{ maxWidth: "200px" }}>
                  <Typography noWrap>{cardData.bankName}</Typography>
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  {renderIcon(cardData.schema)}
                  <Typography>{cardData.type}</Typography>
                </div>
              </div>
              <Typography>
                {cardData.cardBalance} {cardData.currency}
              </Typography>
              <Typography>
                {isActive
                  ? separateMask(cardData.cardNumber)
                  : hideMask(cardData.cardNumber)}
              </Typography>
              <Typography>{cardData.expireDate}</Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item>
        <Button
          sx={{ alignSelf: "flex-start" }}
          onClick={() => removeCard(cardData.cardNumber)}
        >
          Видалити
        </Button>
      </Grid>
    </Grid>
  );
};
