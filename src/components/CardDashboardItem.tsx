import { Button, Grid, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { FC } from "react";
import { useNavigate } from "react-router-dom";
import { IBankCard } from "../types/IBankCard";

interface Props {
  card: IBankCard;
}

const useStyles = makeStyles({
  bankNameTitle: {
    maxWidth: "100px",
  },
  cardBalance: {
    height: "100%",
  },
});

export const CardDashboardItem: FC<Props> = (props) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const { card } = props;
  return (
    <Grid container>
      <Grid item container alignItems="center" columnSpacing={2} xs={7}>
        <Grid item className={classes.bankNameTitle}>
          <Typography noWrap align="left">
            - {card.bankName}
          </Typography>
        </Grid>
        <Grid item>
          <Typography align="left" noWrap className={classes.cardBalance}>
            {card.cardBalance} {card.currency}
          </Typography>
        </Grid>
      </Grid>
      <Grid item xs={5}>
        <Button
          onClick={() =>
            navigate(`/edit-card/${card.cardNumber}`, { replace: true })
          }
        >
          Редагувати
        </Button>
      </Grid>
    </Grid>
  );
};
